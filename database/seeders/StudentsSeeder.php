<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Ramsey\Uuid\Type\Integer;

class StudentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Student::factory(3)->sequence([
            ['name' => 'Sandeepa'],
            ['name' => 'Kusal'],
            ['name' => 'Haritha'],
            ['name' => 'Sandeepa'],
        ]);
    }
}
