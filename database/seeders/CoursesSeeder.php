<?php

namespace Database\Seeders;

use App\Models\Course;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CoursesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Course::factory(6)->sequence(
            ['name' => 'Java'],
            ['name' => 'Python'],
            ['name' => 'React'],
            ['name' => 'Javascript'],
            ['name' => 'PHP'],
            ['name' => 'SQL'],
        )->create();
    }
}
