<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class Student extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'first_name',
        'last_name',
        'date_of_birth',
        'email',
        'phone_number',
        'course_id'
    ];

    public function course(){
        return $this->belongsTo(Course::class);
    }
}
