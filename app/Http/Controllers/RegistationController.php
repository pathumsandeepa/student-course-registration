<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegistationController extends Controller
{

    public function create()
    {
        $courses = Course::all();
        return view('student.registation', [
            'courses' => $courses
        ]);
    }

    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'first_name' => ['required', 'max:100'],
            'last_name' => ['required'],
            'dob' => ['required', 'date'],
            'email' => ['required', 'email'],
            'phone' => ['required', 'max:10'],
            'subject' => ['required'],
        ]);

            $post = new Student();
            $post->first_name = $request->first_name;
            $post->last_name = $request->last_name;
            $post->date_of_birth = $request->dob;
            $post->email = $request->email;
            $post->phone_number = $request->phone;
            $post->course_id = $request->subject;
            $post->save();
            return redirect('/students');
    }




}

