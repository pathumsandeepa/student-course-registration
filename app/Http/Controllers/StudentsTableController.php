<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Student;
use Illuminate\Http\Request;

class StudentsTableController extends Controller
{
    public function index()
    {
        $students = Student::all();
        $courses = Course::all();
        return view('tables.tableView', [
            'students' =>  $students,
            'courses' => $courses,
        ]);
    }
}
