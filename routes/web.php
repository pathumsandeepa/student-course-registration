<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegistationController;
use App\Http\Controllers\StudentsTableController;

Route::get('/', function () {
    return view('welcome');
});

//registration form routers
Route::get('/registration',[RegistationController::class, 'create'])->name('registration');
Route::post('/register',[RegistationController::class, 'store'])->name('reg-data');

//table page routers
Route::get('/students', [StudentsTableController::class, 'index'])->name('table-view-data');
// new project
