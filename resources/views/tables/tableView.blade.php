<!DOCTYPE html>
<html>
<head>
    <title>Students Table</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
        .table-view {
            background: none;
            color: transparent;
            font-size: 50px;
            text-align: center;
            padding: 30px 0;
            background-image: linear-gradient(to right, #FFB6C1, #87CEEB);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-family: Arial black, sans-serif;
        }
        ::selection {
            background-color: black;
            color: yellow;
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button {
            color: white !important;
        }
        .size{
            padding-right: 75px;
            padding-left: 75px;
        }
        .size-btn{
            margin-left: 75px;
        }
    </style>
</head>
<body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
<link href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css" rel="stylesheet">
<div class="table-view">Students Details</div>
<div class="mx-auto mt-1 size">
    <table id="students-table" class="table table-striped table-bordered">
        <thead style="font-family: Arial, sans-serif; background-color: black; color: white;">
        <tr>
            <th>First name</th>
            <th>Last name</th>
            <th>Date of birth</th>
            <th>Email</th>
            <th>Phone number</th>
            <th>Course name</th>
        </tr>
        </thead>
        <tbody id="students-table-body">
        @foreach ($students as $student)
            <tr>
                <td>{{ $student->first_name }}</td>
                <td>{{ $student->last_name }}</td>
                <td>{{ $student->date_of_birth }}</td>
                <td>{{ $student->email }}</td>
                <td>{{ $student->phone_number }}</td>
                <td>{{ $student->course->name }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<a href="{{ url('registration') }}">
    <button type="button" class="btn btn-success size-btn" style="margin-top: 20px;">
        Add new user
    </button>
</a>
<script>
    $(document).ready(function() {
        $('#students-table').DataTable();
    });
</script>
</body>
</html>
